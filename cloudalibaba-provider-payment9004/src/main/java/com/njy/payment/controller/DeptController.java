package com.njy.payment.controller;

import com.njy.payment.service.DeptService;
import com.njy.pojo.CommonResult;
import com.njy.pojo.Dept;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class DeptController {

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private DeptService deptService;

    @GetMapping("/dept/list")
    public CommonResult<List<Dept>> getAllDept(){

        List<Dept> depts = deptService.getAllDept();
        CommonResult<List<Dept>> result = new CommonResult(200, "from mysql,serverPort:  " + serverPort, depts);
        return result;
    }

    @GetMapping("/dept/get/{id}")
    public CommonResult<Dept> getDeptById(@PathVariable(value = "id")Long id){
        log.info("端口：" + serverPort + "\t+ dept/get/");
        /*try {
            TimeUnit.SECONDS.sleep(1);
            log.info("休眠 1秒");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        Dept dept = deptService.getDeptById(id);
        return new CommonResult<Dept>(200,"from mysql,serverPort:  " + serverPort,dept);
    }

}
