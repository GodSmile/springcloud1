package com.njy.payment.mapper;

import com.njy.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DeptMapper {

    List<Dept> getAllDept();

    Dept getDeptById(@Param(value = "id") Long id);

}
