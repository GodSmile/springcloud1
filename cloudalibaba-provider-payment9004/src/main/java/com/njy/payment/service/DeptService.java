package com.njy.payment.service;

import com.njy.payment.mapper.DeptMapper;
import com.njy.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class DeptService {
    @Autowired
    private DeptMapper deptMapper;

    public List<Dept> getAllDept(){
        return deptMapper.getAllDept();
    }

    public Dept getDeptById(Long id){
        return deptMapper.getDeptById(id);
    }
}
