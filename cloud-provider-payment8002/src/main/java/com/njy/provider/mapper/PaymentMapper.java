package com.njy.provider.mapper;

import com.njy.pojo.Payment;

public interface PaymentMapper {


    public Payment getPaymentById(Long id);

}
