package com.njy.provider.service;

import com.njy.pojo.Payment;

public interface PaymentService {
    public Payment getPaymentById(Long id);
}
