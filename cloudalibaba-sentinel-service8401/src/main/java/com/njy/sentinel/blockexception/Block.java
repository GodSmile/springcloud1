package com.njy.sentinel.blockexception;

import com.alibaba.csp.sentinel.slots.block.BlockException;

public class Block {

    /**
     * 不能单独使用，必须与 blockHandler 属性配合使用；
     * 该属性指定的类中的 blockHandler 函数必须为 static 函数，否则无法解析。
     * @param blockException
     * @return
     */
    public static String test(BlockException blockException){
        return "服务访问失败! 您已被限流，请稍后重试--->不在同一个类中";
    }

}
