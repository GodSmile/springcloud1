package com.njy.consumer.feign;

import com.njy.pojo.CommonResult;
import com.njy.pojo.Dept;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class PaymentImpl implements Payment{

    @Override
    public CommonResult<Dept> getDeptById(Long id) {
        return new CommonResult<Dept>(200,"getDeptById进入feign熔断方法",null);
    }

    @Override
    public CommonResult<List<Dept>> getAllDept() {
        return new CommonResult<List<Dept>>(200,"getAllDept进入feign熔断方法",null);
    }
}
