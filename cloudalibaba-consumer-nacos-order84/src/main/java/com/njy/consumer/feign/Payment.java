package com.njy.consumer.feign;

import com.njy.pojo.CommonResult;
import com.njy.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
@FeignClient(value = "spring-cloud-alibaba-provider-mysql",fallback = PaymentImpl.class)
public interface Payment {
    @GetMapping("/dept/get/{id}")
    public CommonResult<Dept> getDeptById(@PathVariable(value = "id")Long id);

    @GetMapping("/dept/list")
    public CommonResult<List<Dept>> getAllDept();
}
