package com.njy.stream.rabbitmq.binding;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputMessageBinding {

    String IN_PUT_NAME = "InPutName";

    @Input(InputMessageBinding.IN_PUT_NAME)
    SubscribableChannel input();
}
