package com.njy.provider.controller;


import com.njy.pojo.CommonResult;
import com.njy.pojo.Payment;
import com.njy.provider.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    @Value("${server.port}")
    private String port;

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable(value = "id") Long id){
        Payment payment = paymentService.getPaymentById(id);
        if(payment != null){
            log.info("查询成功");
            return new CommonResult(200,"调用"+port+"查询成功",payment);
        }
        return new CommonResult<>(444,"没有对应记录,查询ID: "+id,null);
    }


    @GetMapping("/lazy/payment/get/{id}")
    public CommonResult<Payment> getPaymentByIdLazy(@PathVariable(value = "id") Long id){
        // 业务逻辑处理正确，但是需要耗费3秒钟
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Payment payment = paymentService.getPaymentById(id);
        if(payment != null){
            log.info("查询成功");
            return new CommonResult(200,"调用"+port+"查询成功",payment);
        }
        return new CommonResult<>(444,"没有对应记录,查询ID: "+id,null);
    }

}
