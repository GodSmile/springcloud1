package com.njy.aop.annotation;


import com.njy.pojo.OperationType;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {
    public String description() default "";
    public OperationType operationType() default OperationType.unknown;
}
