package com.njy.aop.controller;


import com.njy.aop.service.aopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class aopController {

    @Autowired
    private aopService aopService;


    @GetMapping("/aop/{id}")
    public String AOP(@PathVariable(value = "id") String id){
        return aopService.aop(id);
    }

}
