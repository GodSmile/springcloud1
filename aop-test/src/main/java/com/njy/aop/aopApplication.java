package com.njy.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
public class aopApplication {
    public static void main(String[] args) {
        SpringApplication.run(aopApplication.class,args);
    }
}
