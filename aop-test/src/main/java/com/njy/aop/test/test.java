package com.njy.aop.test;

import nuonuo.open.sdk.NNOpenSDK;

import java.util.UUID;

public class test {
    public static void main(String[] args) {
        NNOpenSDK sdk = NNOpenSDK.getIntance();
        String taxnum = "339901999999500"; // 授权企业税号
        String appKey = "46282132";
        String appSecret = "BBF40998C6DE4603";
        String method = "nuonuo.ElectronInvoice.queryInvoiceResult"; // API方法名
        String token =  "370743d065d1e97e3c12431k8acwrjwi"; // 访问令牌
        String content = "{\n" +
                "  \"isOfferInvoiceDetail\": \"1\",\n" +
                "  \"orderNos\": [],\n" +
                "  \"serialNos\": [\n" +
                "    \"22021514401003909997\"\n" +
                "  ]\n" +
                "}";
        String url = "https://sdk.nuonuo.com/open/v1/services"; // SDK请求地址
        String senid = UUID.randomUUID().toString().replace("-", ""); // 唯一标识，32位随机码，无需修改，保持默认即可
        String result = sdk.sendPostSyncRequest(url, senid, appKey, appSecret, token, taxnum, method, content);
        System.out.println(result);
    }

}
