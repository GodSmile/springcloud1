package com.njy.aop.test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class test4 {
    public static void main(String[] args) {
        String s = "1645407812000";
        Date date = new Date();
        long l = Long.parseLong(s);
        date.setTime(l);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(date);
        System.out.println(format);

        System.out.println(String.valueOf(new Date()));
    }
}
