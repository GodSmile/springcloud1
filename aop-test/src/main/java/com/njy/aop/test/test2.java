package com.njy.aop.test;


import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;

import java.net.URL;
import java.util.*;

public class test2 {
    public static void main(String[] args) {
       test2 t = new test2();
       t.test();
    }

    public void test(){


       /* String content = "{\n" +
                "    \"order\":{\n" +
                "        \"buyerName\":\"正阳县梁娟洁具有限公司\",\n" +
                "        \"salerTaxNum\":\"339901999999500\",\n" +
                "        \"salerTel\":\"0571-77777777\",\n" +
                "        \"salerAddress\":\"佛山市高明区沧江工业园东园三洲园区\",\n" +
                "        \"orderNo\":\"20170105120207971658\",\n" +
                "        \"invoiceDate\":\"2016-01-13 12:30:00\",\n" +
                "        \"clerk\":\"张三\",\n" +
                "        \"buyerPhone\":\"15858585858\",\n" +
                "        \"email\":\"test@xx.com\",\n" +
                "        \"invoiceType\":\"1\",\n" +
                "        \"invoiceLine\":\"c\",\n"+
                "        \"invoiceDetail\":[\n" +
                "            {\n" +
                "                \"goodsName\":\"电脑\",\n" +
                "                \"specType\":\"y460\",\n" +
                "                \"taxExcludedAmount\":\"0.88\",\n" +
                "                \"invoiceLineProperty\":\"0\",\n" +
                "                \"favouredPolicyName\":\"0\",\n" +
                "                \"num\":\"1\",\n" +
                "                \"withTaxFlag\":\"1\",\n" +
                "                \"tax\":\"0.12\",\n" +
                "                \"favouredPolicyFlag\":\"0\",\n" +
                "                \"taxRate\":\"0.13\",\n" +
                "                \"unit\":\"台\",\n" +
                "                \"deduction\":\"0\",\n" +
                "                \"price\":\"1\",\n" +
                "                \"zeroRateFlag\":\"0\",\n" +
                "                \"goodsCode\":\"1090511030000000000\",\n" +
                "                \"selfCode\":\"130005426000000000\",\n" +
                "                \"taxIncludedAmount\":\"1\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";*/
        String content = "{\n" +
                "    \"order\":{\n" +
                "        \"buyerName\":\"佛山市法恩安华卫浴有限公司\",\n" +
                "        \"salerTaxNum\":\"339901999999500\",\n" +
                "        \"salerTel\":\"0571-77777777\",\n" +
                "        \"salerAddress\":\"中国工商银行股份有限公司佛山三水支行\",\n" +
                "        \"orderNo\":\"20170105120207971667\",\n" +
                "        \"invoiceDate\":\"2016-01-13 12:30:00\",\n" +
                "        \"clerk\":\"陈允梦\",\n" +
                "        \"buyerPhone\":\"15858585858\",\n" +
                "        \"email\":\"test@xx.com\",\n" +
                "        \"invoiceType\":\"1\",\n" +
                "        \"buyerTaxNum\":\"914406005901243877\",\n" +
                "        \"buyerAccount\":\"中国农业银行同华支行44423301040067895\",\n" +
                "        \"checker\":\"梁琼丹\",\n" +
                "        \"payee\":\"戴丽丽\",\n" +
                "        \"invoiceLine\":\"c\",\n" +
                "        \"invoiceDetail\":[\n" +
                "            {\"goodsName\":\"二三四五件套浴缸\",\n" +
                "                \"withTaxFlag\":\"1\",\n" +
                "                \"taxRate\":\"0.13\",\n" +
                "                \"goodsCode\":\"\",\n" +
                "                \"price\":\"1944\",\n" +
                "                \"num\":\"12\",\n" +
                "                \"unit\":\"PCS\",\n" +
                "                \"specType\":\"1600*750*580\",\n" +
                "                \"tax\":\"2683.75\",\n" +
                "                \"favouredPolicyFlag\":\"0\",\n" +
                "                \"zeroRateFlag\":\"0\"}\n" +
                "        ]\n" +
                "    }\n" +
                "}";
//        String content ="{\n" +
//                "    \"order\":{\n" +
//                "        \"buyerName\":\"佛山市法恩安华卫浴有限公司\",\n" +
//                "        \"salerTaxNum\":\"339901999999500\",\n" +
//                "        \"salerTel\":\"\",\n" +
//                "        \"salerAddress\":\"中国工商银行股份有限公司佛山三水支行\",\n" +
//                "        \"orderNo\":\"20170105120207971700\",\n" +
//                "        \"invoiceDate\":\"2021-05-17 17:02:24\",\n" +
//                "        \"clerk\":\"陈允梦\",\n" +
//                "        \"buyerPhone\":\"15858585858\",\n" +
//                "        \"email\":\"test@xx.com\",\n" +
//                "        \"invoiceType\":\"1\",\n" +
//                "        \"buyerTaxNum\":\"914406005901243877\",\n" +
//                "        \"buyerTel\":\"\",\n" +
//                "        \"buyerAddress\":\"\",\n" +
//                "        \"buyerAccount\":\"中国农业银行同华支行44423301040067895\",\n" +
//                "        \"invoiceCode\":\"\",\n" +
//                "        \"invoiceNum\":\"\",\n" +
//                "        \"checker\":\"梁琼丹\",\n" +
//                "        \"payee\":\"戴丽丽\",\n" +
//                "        \"invoiceLine\":\"\",\n" +
//                "        \"invoiceDetail\":[\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1944\",\n" +
//                "                \"num\":\"12\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1600*750*580\",\n" +
//                "                \"tax\":\"2683.75\",\n" +
//                "                \"taxExcludedAmount\":\"20644.25\",\n" +
//                "                \"taxIncludedAmount\":\"23328.0\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2132.4\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1600*800*680\",\n" +
//                "                \"tax\":\"245.32\",\n" +
//                "                \"taxExcludedAmount\":\"1887.08\",\n" +
//                "                \"taxIncludedAmount\":\"2132.4\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2063.4\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"脚部加装挡板，成都订（琉璃）\",\n" +
//                "                \"tax\":\"237.38\",\n" +
//                "                \"taxExcludedAmount\":\"1826.02\",\n" +
//                "                \"taxIncludedAmount\":\"2063.4\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2063.4\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"脚部加装挡板，银川订（翡翠公园9-1-10）\",\n" +
//                "                \"tax\":\"237.38\",\n" +
//                "                \"taxExcludedAmount\":\"1826.02\",\n" +
//                "                \"taxIncludedAmount\":\"2063.4\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1846.5\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"短裙反方向，齐齐哈尔订\",\n" +
//                "                \"tax\":\"212.43\",\n" +
//                "                \"taxExcludedAmount\":\"1634.07\",\n" +
//                "                \"taxIncludedAmount\":\"1846.5\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1816.8\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"装去水，常州订（溧阳分销）\",\n" +
//                "                \"tax\":\"209.01\",\n" +
//                "                \"taxExcludedAmount\":\"1607.79\",\n" +
//                "                \"taxIncludedAmount\":\"1816.8\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1710.6\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1400*780*630\",\n" +
//                "                \"tax\":\"196.79\",\n" +
//                "                \"taxExcludedAmount\":\"1513.81\",\n" +
//                "                \"taxIncludedAmount\":\"1710.6\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1702.5\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"装去水，无锡订\",\n" +
//                "                \"tax\":\"195.86\",\n" +
//                "                \"taxExcludedAmount\":\"1506.64\",\n" +
//                "                \"taxIncludedAmount\":\"1702.5\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1246.5\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"装去水，苏州订\",\n" +
//                "                \"tax\":\"143.4\",\n" +
//                "                \"taxExcludedAmount\":\"1103.10\",\n" +
//                "                \"taxIncludedAmount\":\"1246.5\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1199.1\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"装去水，短裙反方向，襄樊订\",\n" +
//                "                \"tax\":\"137.95\",\n" +
//                "                \"taxExcludedAmount\":\"1061.15\",\n" +
//                "                \"taxIncludedAmount\":\"1199.1\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1159.2\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"装去水，肇庆订（发广宁）\",\n" +
//                "                \"tax\":\"133.36\",\n" +
//                "                \"taxExcludedAmount\":\"1025.84\",\n" +
//                "                \"taxIncludedAmount\":\"1159.2\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1842.3\",\n" +
//                "                \"num\":\"5\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1600*800*630\",\n" +
//                "                \"tax\":\"1059.73\",\n" +
//                "                \"taxExcludedAmount\":\"8151.77\",\n" +
//                "                \"taxIncludedAmount\":\"9211.5\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1252.22057516\",\n" +
//                "                \"num\":\"0.6093016\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1508*2000*8\",\n" +
//                "                \"tax\":\"87.78\",\n" +
//                "                \"taxExcludedAmount\":\"675.20\",\n" +
//                "                \"taxIncludedAmount\":\"762.98\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1101.10937888\",\n" +
//                "                \"num\":\"0.6397003\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1326*2000*8\",\n" +
//                "                \"tax\":\"81.03\",\n" +
//                "                \"taxExcludedAmount\":\"623.35\",\n" +
//                "                \"taxIncludedAmount\":\"704.38\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1028.09124115\",\n" +
//                "                \"num\":\"0.6089051\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1511*2000*8\",\n" +
//                "                \"tax\":\"72.02\",\n" +
//                "                \"taxExcludedAmount\":\"553.99\",\n" +
//                "                \"taxIncludedAmount\":\"626.01\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1025.34644463\",\n" +
//                "                \"num\":\"0.6092965\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1507*2000*8\",\n" +
//                "                \"tax\":\"71.87\",\n" +
//                "                \"taxExcludedAmount\":\"552.87\",\n" +
//                "                \"taxIncludedAmount\":\"624.74\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1021.31245021\",\n" +
//                "                \"num\":\"0.6097938\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1501*2000*8\",\n" +
//                "                \"tax\":\"71.65\",\n" +
//                "                \"taxExcludedAmount\":\"551.14\",\n" +
//                "                \"taxIncludedAmount\":\"622.79\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"947.80020322\",\n" +
//                "                \"num\":\"0.6328971\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1393*2000*8\",\n" +
//                "                \"tax\":\"69.01\",\n" +
//                "                \"taxExcludedAmount\":\"530.85\",\n" +
//                "                \"taxIncludedAmount\":\"599.86\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"354.40116279\",\n" +
//                "                \"num\":\"0.2555026\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1495*下宽1495*2000*8\",\n" +
//                "                \"tax\":\"40.77\",\n" +
//                "                \"taxExcludedAmount\":\"313.63\",\n" +
//                "                \"taxIncludedAmount\":\"354.4\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"321.03461538\",\n" +
//                "                \"num\":\"0.2820993\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1300*下宽1300*2000*8\",\n" +
//                "                \"tax\":\"36.93\",\n" +
//                "                \"taxExcludedAmount\":\"284.10\",\n" +
//                "                \"taxIncludedAmount\":\"321.03\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"307.33430232\",\n" +
//                "                \"num\":\"0.3038075\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z1000*X1000*H1900*固6活6\",\n" +
//                "                \"tax\":\"35.36\",\n" +
//                "                \"taxExcludedAmount\":\"271.97\",\n" +
//                "                \"taxIncludedAmount\":\"307.33\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"275.42307693\",\n" +
//                "                \"num\":\"0.2227828\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z1000*X1000*H2200*8\",\n" +
//                "                \"tax\":\"31.69\",\n" +
//                "                \"taxExcludedAmount\":\"243.73\",\n" +
//                "                \"taxIncludedAmount\":\"275.42\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1846.8\",\n" +
//                "                \"num\":\"4\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1700*805*630\",\n" +
//                "                \"tax\":\"849.85\",\n" +
//                "                \"taxExcludedAmount\":\"6537.35\",\n" +
//                "                \"taxIncludedAmount\":\"7387.2\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"272.76453489\",\n" +
//                "                \"num\":\"0.2301053\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z1090*X1005*H2000*8\",\n" +
//                "                \"tax\":\"31.38\",\n" +
//                "                \"taxExcludedAmount\":\"241.38\",\n" +
//                "                \"taxIncludedAmount\":\"272.76\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"264.18072288\",\n" +
//                "                \"num\":\"0.2410873\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z1000*X1000*H1950*8\",\n" +
//                "                \"tax\":\"30.39\",\n" +
//                "                \"taxExcludedAmount\":\"233.79\",\n" +
//                "                \"taxIncludedAmount\":\"264.18\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"261.54941859\",\n" +
//                "                \"num\":\"0.2085037\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1515*2000*8\",\n" +
//                "                \"tax\":\"30.09\",\n" +
//                "                \"taxExcludedAmount\":\"231.46\",\n" +
//                "                \"taxIncludedAmount\":\"261.55\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"252.09593022\",\n" +
//                "                \"num\":\"0.2535139\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z900*X900*H2000*8\",\n" +
//                "                \"tax\":\"29.0\",\n" +
//                "                \"taxExcludedAmount\":\"223.10\",\n" +
//                "                \"taxIncludedAmount\":\"252.1\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"250.62209301\",\n" +
//                "                \"num\":\"0.1997971\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1166*2000*8\",\n" +
//                "                \"tax\":\"28.83\",\n" +
//                "                \"taxExcludedAmount\":\"221.79\",\n" +
//                "                \"taxIncludedAmount\":\"250.62\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"246.84230769\",\n" +
//                "                \"num\":\"0.1283972\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1500*2050*8\",\n" +
//                "                \"tax\":\"28.4\",\n" +
//                "                \"taxExcludedAmount\":\"218.44\",\n" +
//                "                \"taxIncludedAmount\":\"246.84\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"233.72093022\",\n" +
//                "                \"num\":\"0.2127847\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1021*2000*8\",\n" +
//                "                \"tax\":\"26.89\",\n" +
//                "                \"taxExcludedAmount\":\"206.83\",\n" +
//                "                \"taxIncludedAmount\":\"233.72\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"229.02692307\",\n" +
//                "                \"num\":\"0.0993015\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z1100*X1200*H2000*8\",\n" +
//                "                \"tax\":\"26.35\",\n" +
//                "                \"taxExcludedAmount\":\"202.68\",\n" +
//                "                \"taxIncludedAmount\":\"229.03\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"219.64615386\",\n" +
//                "                \"num\":\"0.1259974\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1460*下宽1460*2000*8\",\n" +
//                "                \"tax\":\"25.27\",\n" +
//                "                \"taxExcludedAmount\":\"194.38\",\n" +
//                "                \"taxIncludedAmount\":\"219.65\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"200.48546511\",\n" +
//                "                \"num\":\"0.1451949\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1733*下宽1733*1950*8\",\n" +
//                "                \"tax\":\"23.07\",\n" +
//                "                \"taxExcludedAmount\":\"177.42\",\n" +
//                "                \"taxIncludedAmount\":\"200.49\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1830.6\",\n" +
//                "                \"num\":\"2\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1500*800*630\",\n" +
//                "                \"tax\":\"421.2\",\n" +
//                "                \"taxExcludedAmount\":\"3240.00\",\n" +
//                "                \"taxIncludedAmount\":\"3661.2\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"198.40384614\",\n" +
//                "                \"num\":\"0.1222\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1500*下宽1500*2000*8\",\n" +
//                "                \"tax\":\"22.82\",\n" +
//                "                \"taxExcludedAmount\":\"175.58\",\n" +
//                "                \"taxIncludedAmount\":\"198.4\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"197.00581395\",\n" +
//                "                \"num\":\"0.1478983\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1630*下宽1630*2000*8\",\n" +
//                "                \"tax\":\"22.66\",\n" +
//                "                \"taxExcludedAmount\":\"174.35\",\n" +
//                "                \"taxIncludedAmount\":\"197.01\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"195.58430232\",\n" +
//                "                \"num\":\"0.1313042\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1632*1900*8\",\n" +
//                "                \"tax\":\"22.5\",\n" +
//                "                \"taxExcludedAmount\":\"173.08\",\n" +
//                "                \"taxIncludedAmount\":\"195.58\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"191.79941859\",\n" +
//                "                \"num\":\"0.1530007\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1534*下宽1534*2000*8\",\n" +
//                "                \"tax\":\"22.07\",\n" +
//                "                \"taxExcludedAmount\":\"169.73\",\n" +
//                "                \"taxIncludedAmount\":\"191.8\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"186.62790699\",\n" +
//                "                \"num\":\"0.0943015\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1700*下宽1700*1950*8\",\n" +
//                "                \"tax\":\"21.47\",\n" +
//                "                \"taxExcludedAmount\":\"165.16\",\n" +
//                "                \"taxIncludedAmount\":\"186.63\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"181.9883721\",\n" +
//                "                \"num\":\"0.1374954\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1525*1950*8\",\n" +
//                "                \"tax\":\"20.94\",\n" +
//                "                \"taxExcludedAmount\":\"161.05\",\n" +
//                "                \"taxIncludedAmount\":\"181.99\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"175.6482558\",\n" +
//                "                \"num\":\"0.1361931\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1500*2000*8\",\n" +
//                "                \"tax\":\"20.21\",\n" +
//                "                \"taxExcludedAmount\":\"155.44\",\n" +
//                "                \"taxIncludedAmount\":\"175.65\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"171.74127906\",\n" +
//                "                \"num\":\"0.1225995\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1920*1900*8\",\n" +
//                "                \"tax\":\"19.76\",\n" +
//                "                \"taxExcludedAmount\":\"151.98\",\n" +
//                "                \"taxIncludedAmount\":\"171.74\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"164.4767442\",\n" +
//                "                \"num\":\"0.1144007\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"上宽1235*下宽1235*1950*8\",\n" +
//                "                \"tax\":\"18.92\",\n" +
//                "                \"taxExcludedAmount\":\"145.56\",\n" +
//                "                \"taxIncludedAmount\":\"164.48\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"148.18604652\",\n" +
//                "                \"num\":\"0.1877864\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1045*1950*8\",\n" +
//                "                \"tax\":\"17.05\",\n" +
//                "                \"taxExcludedAmount\":\"131.14\",\n" +
//                "                \"taxIncludedAmount\":\"148.19\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1784.4\",\n" +
//                "                \"num\":\"2\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1300*800*800\",\n" +
//                "                \"tax\":\"410.57\",\n" +
//                "                \"taxExcludedAmount\":\"3158.23\",\n" +
//                "                \"taxIncludedAmount\":\"3568.8\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴房\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"144.909375\",\n" +
//                "                \"num\":\"0.1103013\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"Z663*X1165*H2000*8\",\n" +
//                "                \"tax\":\"16.67\",\n" +
//                "                \"taxExcludedAmount\":\"128.24\",\n" +
//                "                \"taxIncludedAmount\":\"144.91\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"140.88461538\",\n" +
//                "                \"num\":\"0.1647951\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1300*2000*8\",\n" +
//                "                \"tax\":\"16.21\",\n" +
//                "                \"taxExcludedAmount\":\"124.67\",\n" +
//                "                \"taxIncludedAmount\":\"140.88\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"140.38953489\",\n" +
//                "                \"num\":\"0.1654944\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1290*2000*8\",\n" +
//                "                \"tax\":\"16.15\",\n" +
//                "                \"taxExcludedAmount\":\"124.24\",\n" +
//                "                \"taxIncludedAmount\":\"140.39\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"135.88846155\",\n" +
//                "                \"num\":\"0.1722025\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1200*2000*8\",\n" +
//                "                \"tax\":\"15.63\",\n" +
//                "                \"taxExcludedAmount\":\"120.26\",\n" +
//                "                \"taxIncludedAmount\":\"135.89\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"118.46511627\",\n" +
//                "                \"num\":\"0.1125923\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1600*2000*8\",\n" +
//                "                \"tax\":\"13.63\",\n" +
//                "                \"taxExcludedAmount\":\"104.84\",\n" +
//                "                \"taxIncludedAmount\":\"118.47\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"115.88076924\",\n" +
//                "                \"num\":\"0.1167005\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1510*2000*8\",\n" +
//                "                \"tax\":\"13.33\",\n" +
//                "                \"taxExcludedAmount\":\"102.55\",\n" +
//                "                \"taxIncludedAmount\":\"115.88\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"59.04941859\",\n" +
//                "                \"num\":\"0.0572023\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1275*1950*8\",\n" +
//                "                \"tax\":\"6.79\",\n" +
//                "                \"taxExcludedAmount\":\"52.26\",\n" +
//                "                \"taxIncludedAmount\":\"59.05\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"58.82267442\",\n" +
//                "                \"num\":\"0.0572972\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1268*1950*8\",\n" +
//                "                \"tax\":\"6.77\",\n" +
//                "                \"taxExcludedAmount\":\"52.05\",\n" +
//                "                \"taxIncludedAmount\":\"58.82\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"58.77906978\",\n" +
//                "                \"num\":\"0.0747926\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1520*1900*固6活6\",\n" +
//                "                \"tax\":\"6.76\",\n" +
//                "                \"taxExcludedAmount\":\"52.02\",\n" +
//                "                \"taxIncludedAmount\":\"58.78\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"17.94767442\",\n" +
//                "                \"num\":\"0.0163029\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1618*2000*8\",\n" +
//                "                \"tax\":\"2.07\",\n" +
//                "                \"taxExcludedAmount\":\"15.88\",\n" +
//                "                \"taxIncludedAmount\":\"17.95\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"仿古浴室柜\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"1663.05\",\n" +
//                "                \"num\":\"2\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"\",\n" +
//                "                \"tax\":\"382.65\",\n" +
//                "                \"taxExcludedAmount\":\"2943.45\",\n" +
//                "                \"taxIncludedAmount\":\"3326.1\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"17.90769231\",\n" +
//                "                \"num\":\"0.0160045\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1495*2200*8\",\n" +
//                "                \"tax\":\"2.06\",\n" +
//                "                \"taxExcludedAmount\":\"15.85\",\n" +
//                "                \"taxIncludedAmount\":\"17.91\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"淋浴屏风\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"17.74709301\",\n" +
//                "                \"num\":\"0.0170007\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1615*1900*8\",\n" +
//                "                \"tax\":\"2.04\",\n" +
//                "                \"taxExcludedAmount\":\"15.71\",\n" +
//                "                \"taxIncludedAmount\":\"17.75\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2588.7\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1500*1500*650\",\n" +
//                "                \"tax\":\"297.82\",\n" +
//                "                \"taxExcludedAmount\":\"2290.88\",\n" +
//                "                \"taxIncludedAmount\":\"2588.7\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2224.2\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"短裙反方向，马鞍山订（金汇18-103）\",\n" +
//                "                \"tax\":\"255.88\",\n" +
//                "                \"taxExcludedAmount\":\"1968.32\",\n" +
//                "                \"taxIncludedAmount\":\"2224.2\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"goodsName\":\"二三四五件套浴缸\",\n" +
//                "                \"withTaxFlag\":\"1\",\n" +
//                "                \"taxRate\":\"0.13\",\n" +
//                "                \"goodsCode\":\"\",\n" +
//                "                \"price\":\"2224.2\",\n" +
//                "                \"num\":\"1\",\n" +
//                "                \"unit\":\"PCS\",\n" +
//                "                \"specType\":\"1700*800*680\",\n" +
//                "                \"tax\":\"255.88\",\n" +
//                "                \"taxExcludedAmount\":\"1968.32\",\n" +
//                "                \"taxIncludedAmount\":\"2224.2\",\n" +
//                "                \"favouredPolicyFlag\":\"0\",\n" +
//                "                \"zeroRateFlag\":\"0\"\n" +
//                "            }\n" +
//                "        ]\n" +
//                "    }\n" +
//                "}";
        System.out.println(content);
        String taxnum = "339901999999500"; ; // 授权企业税号
        String appKey = "46282132";
        String appSecret = "BBF40998C6DE4603";
        String method = "nuonuo.ElectronInvoice.requestBillingNew"; // API方法名
        // String url = "https://sandbox.nuonuocs.cn/open/v1/services"; // SDK请求地址
        String url = "https://sdk.nuonuo.com/open/v1/services";
        String senid = UUID.randomUUID().toString().replace("-", ""); // 唯一标识，32位随机码，无需修改，保持默认即可
        String token = "370743d065d1e97e3c12431k8acwrjwi";
        String result = this.sendPostSyncRequest(url, senid, appKey, appSecret, token, taxnum, method, content);

        System.out.println(result);
    }
    public String sendPostSyncRequest(String url, String senid, String appKey, String appSecret,
                                      String token, String taxnum, String method, String content) {
        this.verify(senid, "senid不能为空");
        this.verify(appKey, "appKey不能为空");
        this.verify(method, "method不能为空");
        this.verify(url, "请求地址URL不能为空");
        this.verify(content, "content不能为空");
        this.verify(appSecret, "appSecret不能为空");
        try {
            String nonce = String.valueOf(this.getNum(8));
            long timestamp = System.currentTimeMillis() / 1000L;
            StringBuffer sb = new StringBuffer(url);
            sb.append("?senid=").append(senid).append("&nonce=").append(nonce).append("&timestamp=").append(timestamp).append("&appkey=").append(appKey);
            Map<String, String> header = this.buildHeader(url, senid, appKey, appSecret, token, taxnum, method, content, nonce, String.valueOf(timestamp), (Map)null);
            String result = this.sendSyncHttp(sb.toString(), header, content);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("发送HTTP请求异常", e);
        }
    }
    private String sendSyncHttp(String url, Map<String, String> header,
                                String body) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        setHeaders(httpPost, header);
        httpPost.setEntity(new StringEntity(body, "UTF-8"));
        CloseableHttpClient httpClient = null;
        String result = null;
        try {
            httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpPost);
            int code = response.getStatusLine().getStatusCode();
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                result = EntityUtils.toString(httpEntity,"utf-8");
            }
            response.close();
            if (code != 200) {
                throw new Exception("发送HTTP请求失败 code=" + code + " result=" + result);
            } else {
                return result;
            }
        } catch (ClientProtocolException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        }
        return null;
    }
    private static void setHeaders(HttpPost httpPost, Map<String, String> header) {
        Set<String> keys = header.keySet();
        Iterator i$ = keys.iterator();

        while(i$.hasNext()) {
            String key = (String)i$.next();
            httpPost.addHeader(key, (String)header.get(key));
        }

    }
    private Map<String, String> buildHeader(String url, String senid, String appKey, String appSecret, String token, String taxnum, String method,
                                            String content, String nonce, String timestamp, Map<String, String> header) throws Exception {
        URL httpUrl = new URL(url);
        String path = httpUrl.getPath();
        if (header == null || ((Map)header).isEmpty()) {
            header = new HashMap();
        }

        ((Map)header).put("Content-Type", "application/json");
        ((Map)header).put("X-Nuonuo-Sign", this.getSign(path, appSecret, appKey, senid, nonce, content, timestamp));
        ((Map)header).put("accessToken", token);
        ((Map)header).put("userTax", taxnum);
        ((Map)header).put("method", method);
        return (Map)header;
    }
    private String getSign(String path, String secret, String appKey, String senid, String nonce, String body, String timestamp) throws Exception {
        body = this.convertToUtf8(body);
        String[] split = path.split("/");
        StringBuffer signStr = new StringBuffer();
        signStr.append("a=" + split[3]).append("&l=" + split[2]).append("&p=" + split[1]).append("&k=" + appKey).
                append("&i=" + senid).append("&n=" + nonce).append("&t=" + timestamp).append("&f=" + body);
        return hmacSha1WithBase64(signStr.toString(), secret);
    }
    private static String hmacSha1WithBase64(String value, String key) {
        try {
            byte[] rawHmac = hmacSha1(value, key);
            byte[] base64Result = Base64.encodeBase64(rawHmac);
            return new String(base64Result, "UTF-8");
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }
    private static byte[] hmacSha1(String value, String key) {
        try {
            byte[] keyBytes = key.getBytes();
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            return mac.doFinal(value.getBytes("utf-8"));
        } catch (Exception var5) {
            throw new RuntimeException(var5);
        }
    }

    private void verify(String v, String msg) {
        if (v == null || v.trim().length() == 0 || v.toLowerCase().trim().equals("null")) {
            throw new RuntimeException(msg);
        }
    }
    public String convertToUtf8(String gbk) throws Exception {
        try {
            byte[] gbkArr = gbk.getBytes("gbk");
            byte[] utf8Arr = (new String(gbkArr, "gbk")).getBytes("utf-8");
            return new String(utf8Arr, "utf-8");
        } catch (Exception var4) {
            throw new Exception("字符编码转换异常", var4);
        }
    }
    /**
     * @param digit 位数
     * @return 随机生成digit位数的数字
     */
    public static long getNum(int digit) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < digit; i++) {
            if (i == 0 && digit > 1)
                str.append(new Random().nextInt(9) + 1);
            else
                str.append(new Random().nextInt(10));
        }
        return Long.valueOf(str.toString());
    }
}
