package com.njy.aop.test;

import nuonuo.open.sdk.NNOpenSDK;

import java.util.UUID;

public class test3 {
    public static void main(String[] args) {
        NNOpenSDK sdk = NNOpenSDK.getIntance();

        //String url = "https://sdk.nuonuo.com/open/v1/services"; // SDK请求地址
        String content = "{\n" +
                "    \"order\":{\n" +
                "        \"terminalNumber\":\"1\",\n" +
                "        \"invoiceDetail\":[{\n" +
                "            \"specType\":\"y460\",\n" +
                "            \"taxExcludedAmount\":\"0.88\",\n" +
                "            \"invoiceLineProperty\":\"0\",\n" +
                "            \"favouredPolicyName\":\"0\",\n" +
                "            \"num\":\"1\",\n" +
                "            \"withTaxFlag\":\"1\",\n" +
                "            \"tax\":\"0.12\",\n" +
                "            \"favouredPolicyFlag\":\"0\",\n" +
                "            \"taxRate\":\"0.13\",\n" +
                "            \"unit\":\"台\",\n" +
                "            \"deduction\":\"0\",\n" +
                "            \"price\":\"1\",\n" +
                "            \"zeroRateFlag\":\"0\",\n" +
                "            \"goodsCode\":\"1090511030000000000\",\n" +
                "            \"selfCode\":\"130005426000000000\",\n" +
                "            \"goodsName\":\"电脑\",\n" +
                "            \"taxIncludedAmount\":\"1\"\n" +
                "        }],\n" +
                "        \"buyerTel\":\"0571-88888888\",\n" +
                "        \"listFlag\":\"0\",\n" +
                "        \"pushMode\":\"1\",\n" +
                "        \"departmentId\":\"9F7E9439CA8B4C60A2FFF3EA3290B088\",\n" +
                "        \"clerkId\":\"\",\n" +
                "        \"remark\":\"备注信息\",\n" +
                "        \"checker\":\"王五\",\n" +
                "        \"payee\":\"李四\",\n" +
                "        \"buyerAddress\":\"杭州市\",\n" +
                "        \"buyerTaxNum\":\"\",\n" +
                "        \"invoiceType\":\"1\",\n" +
                "        \"invoiceLine\":\"p\",\n" +
                "        \"secondHandCarInfo\":{\n" +
                "            \"organizeType\":\"1\",\n" +
                "            \"vehicleManagementName\":\"杭州\",\n" +
                "            \"sellerPhone\":\"13888888888\",\n" +
                "            \"sellerName\":\"张三\",\n" +
                "            \"brandModel\":\"宝马3系\",\n" +
                "            \"vehicleCode\":\"LHGK43284342384234\",\n" +
                "            \"licenseNumber\":\"浙A12345\",\n" +
                "            \"registerCertNo\":\"330022123321\",\n" +
                "            \"sellerAddress\":\"杭州文一路888号\",\n" +
                "            \"vehicleType\":\"轿车\",\n" +
                "            \"intactCerNum\":\"\",\n" +
                "            \"sellerTaxnum\":\"330100199001010000\"\n" +
                "        },\n" +
                "        \"email\":\"test@xx.com\",\n" +
                "        \"salerAccount\":\"\",\n" +
                "        \"salerTel\":\"0571-77777777\",\n" +
                "        \"orderNo\":\"20170105120207971597\",\n" +
                "        \"callBackUrl\":\"http:127.0.0.1/invoice/callback/\",\n" +
                "        \"machineCode\":\"661288888888\",\n" +
                "        \"billInfoNo\":\"1403011904008472\",\n" +
                "        \"vehicleInfo\":{\n" +
                "            \"taxOfficeCode\":\"13399000\",\n" +
                "            \"manufacturerName\":\"华晨宝马汽车生产有限公司\",\n" +
                "            \"importCerNum\":\"\",\n" +
                "            \"certificate\":\"WDL042613263551\",\n" +
                "            \"engineNum\":\"10111011111\",\n" +
                "            \"taxOfficeName\":\"杭州税务\",\n" +
                "            \"brandModel\":\"宝马3系\",\n" +
                "            \"productOrigin\":\"北京\",\n" +
                "            \"vehicleCode\":\"LHGK43284342384234\",\n" +
                "            \"maxCapacity\":\"5\",\n" +
                "            \"intactCerNum\":\"\",\n" +
                "            \"tonnage\":\"2\",\n" +
                "            \"insOddNum\":\"\",\n" +
                "            \"idNumOrgCode\":\"9114010034683511XD\",\n" +
                "            \"vehicleType\":\"轿车\"\n" +
                "        },\n" +
                "        \"vehicleFlag\":\"1\",\n" +
                "        \"buyerName\":\"企业名称/个人\",\n" +
                "        \"invoiceDate\":\"2016-01-13 12:30:00\",\n" +
                "        \"invoiceCode\":\"125999915630\",\n" +
                "        \"invoiceNum\":\"00130865\",\n" +
                "        \"hiddenBmbbbh\":\"0\",\n" +
                "        \"salerAddress\":\"佛山市高明区沧江工业园东园三洲园区\",\n" +
                "        \"clerk\":\"张三\",\n" +
                "        \"buyerPhone\":\"15858585858\",\n" +
                "        \"buyerAccount\":\"中国工商银行 111111111111\",\n" +
                "        \"productOilFlag\":\"0\",\n" +
                "        \"extensionNumber\":\"0\",\n" +
                "        \"salerTaxNum\":\"339901999999500\",\n" +
                "        \"listName\":\"详见销货清单\",\n" +
                "        \"proxyInvoiceFlag\":\"0\"\n" +
                "    }\n" +
                "}";
        String taxnum = "339901999999500"; // 授权企业税号
        String appKey = "46282132";
        String appSecret = "BBF40998C6DE4603";
        String method = "nuonuo.ElectronInvoice.requestBillingNew"; // API方法名
        String token = "370743d065d1e97e3c12431k8acwrjwi"; // 访问令牌
        String url = "https://sdk.nuonuo.com/open/v1/services"; // SDK请求地址
        String senid = UUID.randomUUID().toString().replace("-", ""); // 唯一标识，32位随机码，无需修改，保持默认即可
        String result = sdk.sendPostSyncRequest(url, senid, appKey, appSecret, token, taxnum, method, content);
        System.out.println(result);
    }
}
