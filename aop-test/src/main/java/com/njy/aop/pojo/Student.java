package com.njy.aop.pojo;

import lombok.ToString;

@ToString

public class Student {
    //必填参数
    private String name;
    private int age;
    //可选参数
    private String email;
    private String address;
    private String phone;

    private Student(StudentBuilder builder){
        this.address = builder.address;
        this.age = builder.age;
        this.email = builder.email;
        this.name = builder.name;
        this.phone = builder.phone;
    }

    public static class  StudentBuilder {
        //必填参数
        private String name;
        private int age;
        //可选参数
        private String email;
        private String address;
        private String phone;
        public StudentBuilder(String name,int age){
            this.name = name;
            this.age = age;
        }
        public StudentBuilder email(String email){
            this.email = email;
            return this;
        }
        public StudentBuilder address(String address){
            this.address = address;
            return this;
        }public StudentBuilder phone(String phone){
            this.phone = phone;
            return this;
        }

        public Student build(){
            return  new Student(this);
        }
    }
}
