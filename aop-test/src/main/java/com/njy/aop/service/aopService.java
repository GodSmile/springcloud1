package com.njy.aop.service;

import com.njy.aop.annotation.SysLog;
import com.njy.pojo.OperationType;
import org.springframework.stereotype.Service;

@Service
public class aopService {
    @SysLog(description = "test",operationType = OperationType.unknown)
    public String aop(String id){
        return "id-->"+id;
    }
}
