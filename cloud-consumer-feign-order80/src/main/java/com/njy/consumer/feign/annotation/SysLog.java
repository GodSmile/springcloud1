package com.njy.consumer.feign.annotation;


import com.njy.pojo.OperationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SysLog {
    public String path();
    public String description() default "";
    public OperationType operationType() default OperationType.unknown;
}
