package com.njy.consumer.feign.controller;



import com.njy.consumer.feign.annotation.SysLog;
import com.njy.consumer.feign.service.FeignPaymentService;
import com.njy.pojo.CommonResult;

import com.njy.pojo.OperationType;
import com.njy.pojo.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignPaymentController {

    @Autowired
    private FeignPaymentService paymentService;

    @GetMapping(value = "/feign/consumer/payment/get/{id}")
    @SysLog(description = "getPaymentById",path = "/feign/consumer/payment/get",operationType = OperationType.select)
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        return paymentService.getPaymentById(id);
    }

    @GetMapping(value = "/feign/lazy/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentByIdLazy(@PathVariable("id") Long id) {
        return paymentService.getPaymentByIdLazy(id);
    }

}
