package com.njy.consumer.feign;


import com.njy.consumer.ribbon.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@RibbonClient(name = "cloud-provider-payment",configuration = RibbonConfig.class)
public class FeignConsumer80Application {
    public static void main(String[] args) {
        SpringApplication.run(FeignConsumer80Application.class,args);
    }
}
