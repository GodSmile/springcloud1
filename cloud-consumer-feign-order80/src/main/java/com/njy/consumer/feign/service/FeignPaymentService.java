package com.njy.consumer.feign.service;


import com.njy.pojo.CommonResult;
import com.njy.pojo.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@FeignClient(value = "cloud-provider-payment")
public interface FeignPaymentService {

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable(value = "id") Long id);


    @GetMapping("/lazy/payment/get/{id}")
    public CommonResult<Payment> getPaymentByIdLazy(@PathVariable(value = "id") Long id);
}
