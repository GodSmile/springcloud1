package com.njy.consumer.ribbon;


import com.njy.pojo.OperationType;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class test {

    public static void main(String[] args) {
       test(1);
    }

    public static String test(int id){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        System.out.println(calendar.getTime());
        return format.format(calendar.getTime())+" id "+id ;

    }
}
