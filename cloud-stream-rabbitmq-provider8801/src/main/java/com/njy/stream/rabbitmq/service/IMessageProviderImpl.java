package com.njy.stream.rabbitmq.service;

import com.njy.stream.rabbitmq.binding.OutputMessageBinding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;

import java.util.UUID;
@EnableBinding(OutputMessageBinding.class) //定义消息的推送管道
public class IMessageProviderImpl implements IMessageProvider{

    @Autowired
    private OutputMessageBinding outputMessageBinding;

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        outputMessageBinding.outPut().send(MessageBuilder.withPayload(serial).build());
        System.out.println("*****serial: "+serial);
        return serial;
    }
}
