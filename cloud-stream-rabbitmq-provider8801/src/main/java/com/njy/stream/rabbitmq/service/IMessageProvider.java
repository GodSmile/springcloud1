package com.njy.stream.rabbitmq.service;

public interface IMessageProvider {
    public String send();
}
