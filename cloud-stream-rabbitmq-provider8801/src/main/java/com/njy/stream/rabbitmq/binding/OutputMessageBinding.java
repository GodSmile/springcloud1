package com.njy.stream.rabbitmq.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OutputMessageBinding {

    /**
     * 配置文件中binders中的名称
     */
    String OUT_PUT_NAME = "outPutName";

    @Output(OutputMessageBinding.OUT_PUT_NAME)
    MessageChannel outPut();
}
