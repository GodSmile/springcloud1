package com.njy.provider.hystrix.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.njy.provider.hystrix.service.HystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HystrixController {

    @Autowired
    private HystrixService hystrixService;

    @GetMapping("/payment/hystrix/getOk/{id}")
    public String getPaymentInfoOK(@PathVariable(value = "id")Integer id){
        return hystrixService.paymentInfo_OK(id);
    }


    @GetMapping("/payment/hystrix/getTimeOut/{id}")
    public String getPaymentTimeOut(@PathVariable(value = "id")Integer id){
        return hystrixService.paymentInfo_TimeOut(id);
    }

    @GetMapping("/payment/hystrix/paymentCircuitBreaker/{id}")
    public String paymentCircuitBreaker(@PathVariable(value = "id")Integer id){
        return hystrixService.paymentCircuitBreaker(id);
    }

}
