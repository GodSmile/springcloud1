package com.njy.pojo;

public enum OperationType {
    unknown("未知"),
    insert("插入"),
    update("更新"),
    select("查询"),
    delete("删除");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    OperationType(String s){
        this.value = s;
    }
}
