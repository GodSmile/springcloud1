package com.njy.pojo;


import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Dept implements Serializable {
    private Long deptNo;
    private String deptName;
    private String dbSource;
}
