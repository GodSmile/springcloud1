package com.njy.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogInfo {
    private String description;
    private String reqParam;
    private String respParam;
    private LocalDateTime creationDate;
    private LocalDateTime finishDate;
    private String cost;
    private String operationType;
    private String method;
    private String path;
}
