package com.njy.consumer.ribbon;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

public interface RibbonRule {

    ServiceInstance getServiceInstance(List<ServiceInstance> serviceInstances);

}
