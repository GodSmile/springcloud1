package com.njy.feign.hystrix.consumer.controller;


import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.njy.feign.hystrix.consumer.service.HystrixPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@DefaultProperties(defaultFallback = "globalFallbackMethod")
public class HystrixConsumerController {

    @Autowired
    private HystrixPaymentService hystrixPaymentService;

    @HystrixCommand
    @GetMapping("/consumer/payment/hystrix/getOk/{id}")
    public String getPaymentInfoOk(@PathVariable(value = "id") Integer id){
        int a = 10/0;
        return hystrixPaymentService.getPaymentInfoOK(id);
    }

    @HystrixCommand(fallbackMethod = "getPaymentTimeOutFallbackMethod",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
    })
    @GetMapping("/consumer/payment/hystrix/getTimeOut/{id}")
    public String getPaymentTimeOut(@PathVariable(value = "id")Integer id){
        return hystrixPaymentService.getPaymentTimeOut(id);
    }

    public String getPaymentTimeOutFallbackMethod(@PathVariable(value = "id")Integer id){
        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己!";
    }


    public String globalFallbackMethod(){
        return "全局fallbackMethod";
    }


}
