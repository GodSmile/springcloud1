package com.njy.feign.hystrix.consumer.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
//@FeignClient(value = "cloud-povider-hystrix-payment8003",fallback = HystrixPaymentFallbackService.class)
@FeignClient(value = "cloud-povider-hystrix-payment8003")
public interface HystrixPaymentService {

    @GetMapping("/payment/hystrix/getOk/{id}")
    public String getPaymentInfoOK(@PathVariable(value = "id")Integer id);

    @GetMapping("/payment/hystrix/getTimeOut/{id}")
    public String getPaymentTimeOut(@PathVariable(value = "id")Integer id);
}
