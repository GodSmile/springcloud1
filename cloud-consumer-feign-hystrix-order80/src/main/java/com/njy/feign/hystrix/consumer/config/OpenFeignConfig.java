package com.njy.feign.hystrix.consumer.config;


import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenFeignConfig {

    @Bean
    Logger.Level logger(){
        return Logger.Level.FULL;
    }

}
