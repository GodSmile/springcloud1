package com.njy.feign.hystrix.consumer.service;


import org.springframework.stereotype.Component;

@Component
public class HystrixPaymentFallbackService implements HystrixPaymentService{
    @Override
    public String getPaymentInfoOK(Integer id) {
        return "HystrixPaymentFallbackService----getPaymentInfoOK";
    }

    @Override
    public String getPaymentTimeOut(Integer id) {
        return "HystrixPaymentFallbackService----getPaymentTimeOut";
    }
}
